#  Copyright (c) 2019 - eGen Guru

import json
import os


def get_password(var):
    path = os.environ[var]
    with open(path + "/passwords.json", encoding='UTF-8') as file:
        data = file.read()
    return json.loads(data)
