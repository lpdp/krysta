#  Copyright (c) 2019 - eGen Guru

from django.apps import AppConfig


class LegacyLpdpConfig(AppConfig):
    name = 'legacy_lpdp'
    verbose_name = 'La Passion des Poèmes'
