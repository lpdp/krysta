#  Copyright (c) 2019 - eGen Guru

# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Scat(models.Model):
    id = models.PositiveSmallIntegerField(db_column='ID', primary_key=True)  # Field name made lowercase.
    parent = models.PositiveIntegerField()
    catname = models.CharField(db_column='CatName', max_length=30)  # Field name made lowercase.
    catlongname = models.CharField(db_column='CatLongName', max_length=80, blank=True,
                                   null=True)  # Field name made lowercase.
    catdesc = models.CharField(db_column='CatDesc', max_length=250)  # Field name made lowercase.
    secread = models.PositiveIntegerField(db_column='SecRead')  # Field name made lowercase.
    secwrite = models.PositiveIntegerField(db_column='SecWrite')  # Field name made lowercase.
    secreply = models.PositiveIntegerField(db_column='SecReply')  # Field name made lowercase.
    secview = models.PositiveIntegerField(db_column='SecView')  # Field name made lowercase.
    secdelete = models.PositiveIntegerField(db_column='SecDelete')  # Field name made lowercase.
    mustmodaprouv = models.IntegerField(db_column='mustModAprouv')  # Field name made lowercase.
    allowimage = models.PositiveIntegerField(db_column='allowImage')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SCat'


class Yearpoemvotes(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    year = models.PositiveSmallIntegerField()
    catid = models.PositiveIntegerField(db_column='CatID', blank=True, null=True)  # Field name made lowercase.
    poemid = models.PositiveIntegerField(db_column='PoemID', blank=True, null=True)  # Field name made lowercase.
    voterid = models.PositiveIntegerField(db_column='VoterID', blank=True, null=True)  # Field name made lowercase.
    ip = models.CharField(db_column='IP', max_length=15)  # Field name made lowercase.
    ronde = models.CharField(max_length=1)
    ts = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'YearPoemVotes'


class Yearpoemvotes2004(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    catid = models.IntegerField(db_column='CatID')  # Field name made lowercase.
    poemid = models.IntegerField(db_column='PoemID')  # Field name made lowercase.
    voterid = models.IntegerField(db_column='VoterID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'YearPoemVotes2004'


class Yearpoemvotes2005(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    catid = models.IntegerField(db_column='CatID')  # Field name made lowercase.
    poemid = models.IntegerField(db_column='PoemID')  # Field name made lowercase.
    voterid = models.IntegerField(db_column='VoterID')  # Field name made lowercase.
    ip = models.CharField(db_column='IP', max_length=15)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'YearPoemVotes2005'


class Yearpoemvotes2006(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    catid = models.IntegerField(db_column='CatID')  # Field name made lowercase.
    poemid = models.IntegerField(db_column='PoemID')  # Field name made lowercase.
    voterid = models.IntegerField(db_column='VoterID')  # Field name made lowercase.
    ip = models.CharField(db_column='IP', max_length=15)  # Field name made lowercase.
    ronde = models.PositiveIntegerField()

    class Meta:
        managed = False
        db_table = 'YearPoemVotes2006'


class Yearpoemvotes2007(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID', blank=True, null=True)  # Field name made lowercase.
    poemid = models.PositiveIntegerField(db_column='PoemID', blank=True, null=True)  # Field name made lowercase.
    voterid = models.PositiveIntegerField(db_column='VoterID', blank=True, null=True)  # Field name made lowercase.
    ip = models.CharField(db_column='IP', max_length=15)  # Field name made lowercase.
    ronde = models.CharField(max_length=1)

    class Meta:
        managed = False
        db_table = 'YearPoemVotes2007'


class Yearpoemvotes2008(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID', blank=True, null=True)  # Field name made lowercase.
    poemid = models.PositiveIntegerField(db_column='PoemID', blank=True, null=True)  # Field name made lowercase.
    voterid = models.PositiveIntegerField(db_column='VoterID', blank=True, null=True)  # Field name made lowercase.
    ip = models.CharField(db_column='IP', max_length=15)  # Field name made lowercase.
    ronde = models.CharField(max_length=1)

    class Meta:
        managed = False
        db_table = 'YearPoemVotes2008'


class Yearpoemvotes2009(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    year = models.PositiveSmallIntegerField()
    catid = models.PositiveIntegerField(db_column='CatID', blank=True, null=True)  # Field name made lowercase.
    poemid = models.PositiveIntegerField(db_column='PoemID', blank=True, null=True)  # Field name made lowercase.
    voterid = models.PositiveIntegerField(db_column='VoterID', blank=True, null=True)  # Field name made lowercase.
    ip = models.CharField(db_column='IP', max_length=15)  # Field name made lowercase.
    ronde = models.CharField(max_length=1)

    class Meta:
        managed = False
        db_table = 'YearPoemVotes2009'


class Yearpoemvotesread(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID', blank=True, null=True)  # Field name made lowercase.
    poemid = models.PositiveIntegerField(db_column='PoemID', blank=True, null=True)  # Field name made lowercase.
    voterid = models.PositiveIntegerField(db_column='VoterID', blank=True, null=True)  # Field name made lowercase.
    ronde = models.CharField(max_length=1)

    class Meta:
        managed = False
        db_table = 'YearPoemVotesRead'
        unique_together = (('voterid', 'catid', 'poemid'),)


class Arevisiter(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    poemid = models.PositiveIntegerField(db_column='poemId')  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='catId')  # Field name made lowercase.
    ownerid = models.PositiveIntegerField(db_column='ownerId')  # Field name made lowercase.
    date = models.DateTimeField()
    note = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'aRevisiter'


class Allopasslog(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    authorid = models.PositiveIntegerField(db_column='authorID')  # Field name made lowercase.
    method = models.CharField(max_length=12)
    codes = models.CharField(max_length=5000, blank=True, null=True)
    oldexp = models.DateTimeField(db_column='oldExp')  # Field name made lowercase.
    newexp = models.DateTimeField(db_column='newExp')  # Field name made lowercase.
    date = models.DateTimeField()
    ip = models.CharField(db_column='IP', max_length=20)  # Field name made lowercase.
    note = models.CharField(max_length=11)

    class Meta:
        managed = False
        db_table = 'allopassLog'


class Allpoems(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    uniquereply = models.PositiveIntegerField(db_column='uniqueReply')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.
    modaprouv = models.CharField(db_column='modAprouv', max_length=1)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'allpoems'
        unique_together = (('id', 'catid'),)


class Allpoemsreply(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'allpoemsReply'


class Alluserdisc(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'alluserdisc'
        unique_together = (('id', 'catid'),)


class Alluserdiscreply(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'alluserdiscReply'


class Avertissements(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    authorid = models.PositiveIntegerField(db_column='authorID')  # Field name made lowercase.
    avertisseurid = models.PositiveIntegerField(db_column='avertisseurID')  # Field name made lowercase.
    postid = models.PositiveIntegerField(db_column='postID')  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='replyID', blank=True, null=True)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='catID')  # Field name made lowercase.
    status = models.CharField(max_length=12)
    date = models.DateTimeField(blank=True, null=True)
    deleteon = models.DateTimeField(db_column='deleteOn', blank=True, null=True)  # Field name made lowercase.
    notes = models.TextField(blank=True, null=True)
    sujet = models.CharField(max_length=150)
    msg = models.TextField(blank=True, null=True)
    textoriginal = models.TextField(db_column='textOriginal')  # Field name made lowercase.
    memberread = models.CharField(db_column='memberRead', max_length=1)  # Field name made lowercase.
    desactivercorrection = models.IntegerField(db_column='desactiverCorrection')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'avertissements'


class Avertissementsactions(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    avertissementid = models.PositiveIntegerField(db_column='avertissementID')  # Field name made lowercase.
    authorid = models.PositiveIntegerField(db_column='authorID')  # Field name made lowercase.
    date = models.DateTimeField(blank=True, null=True)
    message = models.TextField()
    actiontype = models.CharField(db_column='actionType', max_length=12, blank=True,
                                  null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'avertissementsActions'


class Avertissementspresets(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    sujet = models.CharField(max_length=50, blank=True, null=True)
    msg = models.TextField(blank=True, null=True)
    graceperiod = models.PositiveIntegerField(db_column='gracePeriod')  # Field name made lowercase.
    nom = models.CharField(max_length=100)
    desactivercorrection = models.IntegerField(db_column='desactiverCorrection')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'avertissementsPresets'


class Awards(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    poemid = models.IntegerField(db_column='PoemID')  # Field name made lowercase.
    catid = models.IntegerField(db_column='CatID')  # Field name made lowercase.
    titre = models.CharField(db_column='Titre', max_length=150)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte')  # Field name made lowercase.
    date = models.DateField(db_column='Date')  # Field name made lowercase.
    awardcat = models.IntegerField(db_column='AwardCat', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'awards'


class Ban(models.Model):
    email = models.CharField(primary_key=True, max_length=50)
    date = models.DateField(blank=True, null=True)
    details = models.CharField(max_length=255, blank=True, null=True)
    bannerid = models.CharField(db_column='bannerID', max_length=25, blank=True,
                                null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ban'


class Cat(models.Model):
    id = models.PositiveSmallIntegerField(db_column='ID', primary_key=True)  # Field name made lowercase.
    catname = models.CharField(db_column='CatName', max_length=30)  # Field name made lowercase.
    catlongname = models.CharField(db_column='CatLongName', max_length=60, blank=True,
                                   null=True)  # Field name made lowercase.
    catdesc = models.CharField(db_column='CatDesc', max_length=250)  # Field name made lowercase.
    ordre = models.PositiveIntegerField(db_column='Ordre', blank=True, null=True)  # Field name made lowercase.
    secread = models.PositiveIntegerField(db_column='SecRead')  # Field name made lowercase.
    secwrite = models.PositiveIntegerField(db_column='SecWrite')  # Field name made lowercase.
    secreply = models.PositiveIntegerField(db_column='SecReply')  # Field name made lowercase.
    secview = models.PositiveIntegerField(db_column='SecView')  # Field name made lowercase.
    secdelete = models.PositiveIntegerField(db_column='SecDelete')  # Field name made lowercase.
    mustmodaprouv = models.IntegerField(db_column='mustModAprouv')  # Field name made lowercase.
    sep = models.PositiveSmallIntegerField(db_column='Sep')  # Field name made lowercase.
    type = models.CharField(db_column='Type', max_length=10)  # Field name made lowercase.
    longdesc = models.TextField(db_column='LongDesc', blank=True, null=True)  # Field name made lowercase.
    allowimage = models.PositiveIntegerField(db_column='allowImage')  # Field name made lowercase.
    hasscat = models.CharField(db_column='hasSCat', max_length=1)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'cat'


class Cocherlu(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveIntegerField(db_column='Author', blank=True, null=True)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID', blank=True, null=True)  # Field name made lowercase.
    date = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cocherLu'


class Comment1(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment1'


class Comment10(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment10'


class Comment11(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment11'


class Comment12(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment12'


class Comment13(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment13'


class Comment14(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment14'


class Comment15(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment15'


class Comment16(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment16'


class Comment17(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment17'


class Comment18(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment18'


class Comment19(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment19'


class Comment2(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment2'


class Comment20(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment20'


class Comment21(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment21'


class Comment22(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment22'


class Comment23(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment23'


class Comment24(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment24'


class Comment3(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment3'


class Comment4(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment4'


class Comment5(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment5'


class Comment6(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment6'


class Comment7(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment7'


class Comment8(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment8'


class Comment9(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replyid = models.PositiveIntegerField(db_column='ReplyID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comment9'


class Correcteurabsent(models.Model):
    userid = models.PositiveIntegerField(db_column='userID', primary_key=True)  # Field name made lowercase.
    since = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'correcteurAbsent'


class Deleterequest(models.Model):
    author = models.PositiveIntegerField(db_column='Author', primary_key=True)  # Field name made lowercase.
    date = models.DateTimeField(db_column='Date')  # Field name made lowercase.
    ip = models.CharField(db_column='IP', max_length=15)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'deleteRequest'


class Demandecorrection(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='catID')  # Field name made lowercase.
    poemid = models.PositiveIntegerField(db_column='poemID')  # Field name made lowercase.
    date = models.DateTimeField()
    authorid = models.PositiveIntegerField(db_column='authorID')  # Field name made lowercase.
    statut = models.CharField(max_length=6)
    checkoutid = models.IntegerField(db_column='checkOutId', blank=True, null=True)  # Field name made lowercase.
    checkoutdate = models.DateTimeField(db_column='checkOutDate', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'demandeCorrection'
        unique_together = (('catid', 'poemid'),)


class Demandecorrectionhistorique(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    demandeid = models.IntegerField(db_column='demandeID')  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='catID')  # Field name made lowercase.
    poemid = models.PositiveIntegerField(db_column='poemID')  # Field name made lowercase.
    date = models.DateTimeField()
    correcteurid = models.PositiveIntegerField(db_column='correcteurID')  # Field name made lowercase.
    beforetxt = models.TextField(db_column='beforeTxt', blank=True, null=True)  # Field name made lowercase.
    newtxt = models.TextField(db_column='newTxt', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'demandeCorrectionHistorique'


class Emotfilter(models.Model):
    avant = models.TextField()
    apres = models.TextField()

    class Meta:
        managed = False
        db_table = 'emotfilter'


class FaqCat(models.Model):
    id = models.PositiveSmallIntegerField(db_column='ID', primary_key=True)  # Field name made lowercase.
    parent_id = models.PositiveSmallIntegerField(db_column='parent_ID')  # Field name made lowercase.
    ordre = models.PositiveSmallIntegerField()
    name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'faq_cat'


class FaqData(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='catID')  # Field name made lowercase.
    keywords = models.CharField(max_length=500)
    sujet = models.CharField(max_length=500)
    txt = models.TextField()
    authorid = models.IntegerField(db_column='authorID')  # Field name made lowercase.
    datum = models.CharField(max_length=15)
    lastedit = models.DateTimeField(db_column='lastEdit')  # Field name made lowercase.
    hidden = models.CharField(max_length=1)

    class Meta:
        managed = False
        db_table = 'faq_data'


class FaqDataArchive(models.Model):
    id = models.PositiveIntegerField(db_column='ID', primary_key=True)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='catID')  # Field name made lowercase.
    keywords = models.CharField(max_length=500)
    sujet = models.CharField(max_length=500)
    txt = models.TextField()
    authorid = models.IntegerField(db_column='authorID')  # Field name made lowercase.
    lastedit = models.DateTimeField(db_column='lastEdit')  # Field name made lowercase.
    hidden = models.CharField(max_length=1)

    class Meta:
        managed = False
        db_table = 'faq_data_archive'


class Favauthor(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    authorid = models.PositiveIntegerField(db_column='authorID')  # Field name made lowercase.
    ownerid = models.PositiveIntegerField(db_column='ownerID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'favAuthor'


class Favorite(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    collectionid = models.PositiveIntegerField(db_column='collectionID')  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    poemid = models.PositiveIntegerField(db_column='PoemID')  # Field name made lowercase.
    txt = models.CharField(max_length=150, blank=True, null=True)
    dateadded = models.DateTimeField(db_column='dateAdded', blank=True, null=True)  # Field name made lowercase.
    ordre = models.PositiveIntegerField()

    class Meta:
        managed = False
        db_table = 'favorite'


class FavoriteCollection(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveIntegerField(db_column='Author')  # Field name made lowercase.
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=500)
    created = models.DateTimeField()
    modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'favorite_collection'


class Fichiers(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='catID')  # Field name made lowercase.
    postid = models.PositiveIntegerField(db_column='postID')  # Field name made lowercase.
    authorid = models.PositiveIntegerField(db_column='AuthorID', blank=True, null=True)  # Field name made lowercase.
    path = models.CharField(max_length=100, blank=True, null=True)
    filename = models.CharField(max_length=50, blank=True, null=True)
    sizekb = models.PositiveIntegerField(db_column='sizeKB')  # Field name made lowercase.
    uploaddate = models.DateTimeField(db_column='uploadDate', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'fichiers'


class G152002(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.SmallIntegerField(db_column='Author')  # Field name made lowercase.
    catid = models.IntegerField(db_column='CatID')  # Field name made lowercase.
    poemid = models.SmallIntegerField(db_column='PoemID')  # Field name made lowercase.
    judge = models.SmallIntegerField(db_column='Judge')  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    vote = models.IntegerField(db_column='Vote')  # Field name made lowercase.
    visite = models.IntegerField(db_column='Visite')  # Field name made lowercase.
    reponse = models.IntegerField(db_column='Reponse')  # Field name made lowercase.
    authorid = models.PositiveIntegerField(db_column='AuthorID')  # Field name made lowercase.
    position = models.PositiveIntegerField()
    save_username = models.CharField(max_length=20)
    save_title = models.CharField(max_length=250)
    save_text = models.TextField()

    class Meta:
        managed = False
        db_table = 'g152002'


class G152003(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    authorid = models.SmallIntegerField(db_column='AuthorID')  # Field name made lowercase.
    catid = models.IntegerField(db_column='CatID')  # Field name made lowercase.
    poemid = models.SmallIntegerField(db_column='PoemID')  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    vote = models.IntegerField(db_column='Vote')  # Field name made lowercase.
    visite = models.IntegerField(db_column='Visite')  # Field name made lowercase.
    reponse = models.IntegerField(db_column='Reponse')  # Field name made lowercase.
    position = models.PositiveIntegerField(blank=True, null=True)
    save_username = models.CharField(max_length=20)
    save_title = models.CharField(max_length=250)
    save_text = models.TextField()

    class Meta:
        managed = False
        db_table = 'g152003'


class G152004(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    catid = models.IntegerField(db_column='CatID')  # Field name made lowercase.
    poemid = models.IntegerField(db_column='PoemID')  # Field name made lowercase.
    authorid = models.IntegerField(db_column='AuthorID')  # Field name made lowercase.
    vote = models.PositiveIntegerField(db_column='Vote')  # Field name made lowercase.
    visite = models.PositiveIntegerField(db_column='Visite')  # Field name made lowercase.
    reponse = models.PositiveIntegerField(db_column='Reponse')  # Field name made lowercase.
    position = models.PositiveIntegerField()
    save_username = models.CharField(max_length=20)
    save_title = models.CharField(max_length=250)
    save_text = models.TextField()

    class Meta:
        managed = False
        db_table = 'g152004'


class G152005(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    catid = models.IntegerField(db_column='CatID')  # Field name made lowercase.
    poemid = models.IntegerField(db_column='PoemID')  # Field name made lowercase.
    authorid = models.IntegerField(db_column='AuthorID')  # Field name made lowercase.
    vote = models.PositiveIntegerField(db_column='Vote')  # Field name made lowercase.
    visite = models.PositiveIntegerField(db_column='Visite')  # Field name made lowercase.
    reponse = models.PositiveIntegerField(db_column='Reponse')  # Field name made lowercase.
    position = models.PositiveIntegerField()
    save_username = models.CharField(max_length=20)
    save_title = models.CharField(max_length=250)
    save_text = models.TextField()

    class Meta:
        managed = False
        db_table = 'g152005'


class G152006(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    catid = models.IntegerField(db_column='CatID')  # Field name made lowercase.
    poemid = models.IntegerField(db_column='PoemID')  # Field name made lowercase.
    authorid = models.IntegerField(db_column='AuthorID')  # Field name made lowercase.
    vote = models.PositiveIntegerField(db_column='Vote')  # Field name made lowercase.
    voternd1 = models.PositiveIntegerField(db_column='VoteRnd1', blank=True, null=True)  # Field name made lowercase.
    visite = models.PositiveIntegerField(db_column='Visite')  # Field name made lowercase.
    reponse = models.PositiveIntegerField(db_column='Reponse')  # Field name made lowercase.
    position = models.PositiveIntegerField(blank=True, null=True)
    save_username = models.CharField(max_length=20)
    positionrnd1 = models.PositiveIntegerField(db_column='positionRnd1', blank=True,
                                               null=True)  # Field name made lowercase.
    save_title = models.CharField(max_length=250)
    save_text = models.TextField()

    class Meta:
        managed = False
        db_table = 'g152006'


class G152007(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    catid = models.IntegerField(db_column='CatID')  # Field name made lowercase.
    poemid = models.IntegerField(db_column='PoemID')  # Field name made lowercase.
    authorid = models.IntegerField(db_column='AuthorID')  # Field name made lowercase.
    vote = models.PositiveIntegerField(db_column='Vote')  # Field name made lowercase.
    voternd1 = models.PositiveIntegerField(db_column='VoteRnd1', blank=True, null=True)  # Field name made lowercase.
    visite = models.PositiveIntegerField(db_column='Visite')  # Field name made lowercase.
    reponse = models.PositiveIntegerField(db_column='Reponse')  # Field name made lowercase.
    position = models.PositiveIntegerField(blank=True, null=True)
    save_username = models.CharField(max_length=20)
    positionrnd1 = models.PositiveIntegerField(db_column='positionRnd1', blank=True,
                                               null=True)  # Field name made lowercase.
    save_title = models.CharField(max_length=250)
    save_text = models.TextField()

    class Meta:
        managed = False
        db_table = 'g152007'


class G152008(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    catid = models.IntegerField(db_column='CatID')  # Field name made lowercase.
    poemid = models.IntegerField(db_column='PoemID')  # Field name made lowercase.
    authorid = models.IntegerField(db_column='AuthorID')  # Field name made lowercase.
    vote = models.PositiveIntegerField(db_column='Vote')  # Field name made lowercase.
    voternd1 = models.PositiveIntegerField(db_column='VoteRnd1', blank=True, null=True)  # Field name made lowercase.
    visite = models.PositiveIntegerField(db_column='Visite')  # Field name made lowercase.
    reponse = models.PositiveIntegerField(db_column='Reponse')  # Field name made lowercase.
    position = models.PositiveIntegerField(blank=True, null=True)
    save_username = models.CharField(max_length=20)
    positionrnd1 = models.PositiveIntegerField(db_column='positionRnd1', blank=True,
                                               null=True)  # Field name made lowercase.
    save_title = models.CharField(max_length=250)
    save_text = models.TextField()

    class Meta:
        managed = False
        db_table = 'g152008'


class G152009(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    catid = models.IntegerField(db_column='CatID')  # Field name made lowercase.
    poemid = models.IntegerField(db_column='PoemID')  # Field name made lowercase.
    authorid = models.IntegerField(db_column='AuthorID')  # Field name made lowercase.
    vote = models.PositiveIntegerField(db_column='Vote')  # Field name made lowercase.
    voternd1 = models.PositiveIntegerField(db_column='VoteRnd1', blank=True, null=True)  # Field name made lowercase.
    visite = models.PositiveIntegerField(db_column='Visite')  # Field name made lowercase.
    reponse = models.PositiveIntegerField(db_column='Reponse')  # Field name made lowercase.
    position = models.PositiveIntegerField(blank=True, null=True)
    save_username = models.CharField(max_length=20)
    positionrnd1 = models.PositiveIntegerField(db_column='positionRnd1', blank=True,
                                               null=True)  # Field name made lowercase.
    save_title = models.CharField(max_length=250)
    save_text = models.TextField()

    class Meta:
        managed = False
        db_table = 'g152009'


class G152010(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    catid = models.IntegerField(db_column='CatID')  # Field name made lowercase.
    poemid = models.IntegerField(db_column='PoemID')  # Field name made lowercase.
    authorid = models.IntegerField(db_column='AuthorID')  # Field name made lowercase.
    vote = models.PositiveIntegerField(db_column='Vote')  # Field name made lowercase.
    voternd1 = models.PositiveIntegerField(db_column='VoteRnd1', blank=True, null=True)  # Field name made lowercase.
    visite = models.PositiveIntegerField(db_column='Visite')  # Field name made lowercase.
    reponse = models.PositiveIntegerField(db_column='Reponse')  # Field name made lowercase.
    position = models.PositiveIntegerField(blank=True, null=True)
    save_username = models.CharField(max_length=20)
    positionrnd1 = models.PositiveIntegerField(db_column='positionRnd1', blank=True,
                                               null=True)  # Field name made lowercase.
    positionrealrnd1 = models.PositiveIntegerField(db_column='positionRealRnd1', blank=True,
                                                   null=True)  # Field name made lowercase.
    save_title = models.CharField(max_length=250)
    save_text = models.TextField()

    class Meta:
        managed = False
        db_table = 'g152010'


class G152011(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    catid = models.IntegerField(db_column='CatID')  # Field name made lowercase.
    poemid = models.IntegerField(db_column='PoemID')  # Field name made lowercase.
    authorid = models.IntegerField(db_column='AuthorID')  # Field name made lowercase.
    vote = models.PositiveIntegerField(db_column='Vote')  # Field name made lowercase.
    voternd1 = models.PositiveIntegerField(db_column='VoteRnd1', blank=True, null=True)  # Field name made lowercase.
    visite = models.PositiveIntegerField(db_column='Visite')  # Field name made lowercase.
    reponse = models.PositiveIntegerField(db_column='Reponse')  # Field name made lowercase.
    position = models.PositiveIntegerField(blank=True, null=True)
    save_username = models.CharField(max_length=20)
    positionrnd1 = models.PositiveIntegerField(db_column='positionRnd1', blank=True,
                                               null=True)  # Field name made lowercase.
    positionrealrnd1 = models.PositiveIntegerField(db_column='positionRealRnd1', blank=True,
                                                   null=True)  # Field name made lowercase.
    save_title = models.CharField(max_length=250)
    save_text = models.TextField()

    class Meta:
        managed = False
        db_table = 'g152011'


class G152012Plus(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    year = models.IntegerField(db_column='Year')  # Field name made lowercase.
    catid = models.IntegerField(db_column='CatID')  # Field name made lowercase.
    poemid = models.IntegerField(db_column='PoemID')  # Field name made lowercase.
    authorid = models.IntegerField(db_column='AuthorID')  # Field name made lowercase.
    vote = models.PositiveIntegerField(db_column='Vote')  # Field name made lowercase.
    voternd1 = models.PositiveIntegerField(db_column='VoteRnd1', blank=True, null=True)  # Field name made lowercase.
    visite = models.PositiveIntegerField(db_column='Visite')  # Field name made lowercase.
    reponse = models.PositiveIntegerField(db_column='Reponse')  # Field name made lowercase.
    position = models.PositiveIntegerField(blank=True, null=True)
    save_username = models.CharField(max_length=20)
    positionrnd1 = models.PositiveIntegerField(db_column='positionRnd1', blank=True,
                                               null=True)  # Field name made lowercase.
    positionrealrnd1 = models.PositiveIntegerField(db_column='positionRealRnd1', blank=True,
                                                   null=True)  # Field name made lowercase.
    save_title = models.CharField(max_length=250)
    save_text = models.TextField()

    class Meta:
        managed = False
        db_table = 'g152012plus'


class G15Poemes(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    catid = models.IntegerField(db_column='CatID')  # Field name made lowercase.
    poemid = models.IntegerField(db_column='PoemID')  # Field name made lowercase.
    authorid = models.IntegerField(db_column='AuthorID')  # Field name made lowercase.
    judgeid = models.IntegerField(db_column='JudgeID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'g15poemes'


class Hasardequipe(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    poemid = models.PositiveIntegerField(db_column='poemID')  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='catID')  # Field name made lowercase.
    date = models.DateTimeField()
    authorid = models.PositiveIntegerField(db_column='authorID')  # Field name made lowercase.
    modid = models.PositiveIntegerField(db_column='modID')  # Field name made lowercase.
    raison = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'hasardEquipe'


class Loggedin(models.Model):
    username = models.PositiveIntegerField(unique=True, blank=True, null=True)
    logid = models.CharField(db_column='LogID', primary_key=True, max_length=50)  # Field name made lowercase.
    ip = models.CharField(db_column='IP', max_length=15, blank=True, null=True)  # Field name made lowercase.
    currstatus = models.CharField(db_column='CurrStatus', max_length=50, blank=True,
                                  null=True)  # Field name made lowercase.
    laston = models.DateTimeField(db_column='LastOn')  # Field name made lowercase.
    type = models.SmallIntegerField(db_column='Type')  # Field name made lowercase.
    invisible = models.PositiveIntegerField()
    sitelocation = models.CharField(db_column='SiteLocation', max_length=250, blank=True,
                                    null=True)  # Field name made lowercase.
    nick = models.CharField(max_length=20, blank=True, null=True)
    useragent = models.CharField(max_length=200, blank=True, null=True)
    hostname = models.CharField(max_length=200, blank=True, null=True)
    level = models.PositiveIntegerField(db_column='Level')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'loggedin'


class Mail(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    parentid = models.PositiveIntegerField(db_column='parentID', blank=True, null=True)  # Field name made lowercase.
    destid = models.PositiveIntegerField(db_column='DestID')  # Field name made lowercase.
    senderid = models.PositiveIntegerField(db_column='SenderID')  # Field name made lowercase.
    readflag = models.CharField(db_column='ReadFlag', max_length=1)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=150)  # Field name made lowercase.
    sentdate = models.DateTimeField(db_column='SentDate', blank=True, null=True)  # Field name made lowercase.
    important = models.IntegerField()
    groupeid = models.IntegerField(db_column='groupeID')  # Field name made lowercase.
    delsender = models.CharField(db_column='delSender', max_length=1)  # Field name made lowercase.
    delreceiver = models.CharField(db_column='delReceiver', max_length=1)  # Field name made lowercase.
    threadid = models.PositiveIntegerField(db_column='threadID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'mail'


class Memberdeletehistory(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    authorid = models.PositiveIntegerField(db_column='authorID')  # Field name made lowercase.
    email = models.CharField(max_length=70)
    username = models.CharField(max_length=20)
    deleter = models.CharField(max_length=20)
    date = models.DateTimeField()
    raison = models.CharField(max_length=11)

    class Meta:
        managed = False
        db_table = 'memberDeleteHistory'


class Members(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    username = models.CharField(db_column='UserName', max_length=20, blank=True,
                                null=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=30)  # Field name made lowercase.
    userflag = models.CharField(db_column='UserFlag', max_length=100, blank=True,
                                null=True)  # Field name made lowercase.
    laston = models.DateTimeField(db_column='LastOn', blank=True, null=True)  # Field name made lowercase.
    lastip = models.CharField(db_column='LastIP', max_length=15, blank=True, null=True)  # Field name made lowercase.
    level = models.PositiveSmallIntegerField(db_column='Level')  # Field name made lowercase.
    poemcount = models.PositiveSmallIntegerField(db_column='PoemCount')  # Field name made lowercase.
    commentcount = models.PositiveSmallIntegerField(db_column='CommentCount')  # Field name made lowercase.
    email = models.CharField(db_column='EMail', unique=True, max_length=320, blank=True,
                             null=True)  # Field name made lowercase.
    logincount = models.PositiveIntegerField(db_column='LoginCount')  # Field name made lowercase.
    pageviews = models.IntegerField(db_column='PageViews')  # Field name made lowercase.
    maxperpage = models.PositiveIntegerField(db_column='MaxPerPage')  # Field name made lowercase.
    password = models.CharField(db_column='Password', max_length=30)  # Field name made lowercase.
    regsince = models.DateField(db_column='RegSince', blank=True, null=True)  # Field name made lowercase.
    activationcode = models.CharField(db_column='ActivationCode', max_length=20, blank=True,
                                      null=True)  # Field name made lowercase.
    signature = models.TextField(blank=True, null=True)
    accounttype = models.CharField(db_column='AccountType', max_length=9)  # Field name made lowercase.
    expiration = models.DateTimeField(db_column='Expiration', blank=True, null=True)  # Field name made lowercase.
    cookies = models.CharField(db_column='Cookies', max_length=3)  # Field name made lowercase.
    defaultcomment = models.CharField(db_column='DefaultComment', max_length=3, blank=True,
                                      null=True)  # Field name made lowercase.
    defaultclassement = models.CharField(db_column='DefaultClassement', max_length=3, blank=True,
                                         null=True)  # Field name made lowercase.
    defaultemailnotification = models.CharField(db_column='DefaultEmailNotification', max_length=3, blank=True,
                                                null=True)  # Field name made lowercase.
    defaultcommentlisting = models.CharField(db_column='DefaultCommentListing', max_length=3, blank=True,
                                             null=True)  # Field name made lowercase.
    displaynewonly = models.CharField(db_column='DisplayNewOnly', max_length=3, blank=True,
                                      null=True)  # Field name made lowercase.
    internalfowarding = models.CharField(db_column='InternalFowarding', max_length=3, blank=True,
                                         null=True)  # Field name made lowercase.
    bdayprivate = models.CharField(db_column='BdayPrivate', max_length=3, blank=True,
                                   null=True)  # Field name made lowercase.
    birthdate = models.CharField(db_column='BirthDate', max_length=10, blank=True,
                                 null=True)  # Field name made lowercase.
    sexe = models.CharField(db_column='Sexe', max_length=2)  # Field name made lowercase.
    lieu = models.CharField(db_column='Lieu', max_length=100, blank=True, null=True)  # Field name made lowercase.
    profession = models.CharField(db_column='Profession', max_length=500, blank=True,
                                  null=True)  # Field name made lowercase.
    favmovie = models.CharField(db_column='FavMovie', max_length=1000, blank=True,
                                null=True)  # Field name made lowercase.
    favmusic = models.CharField(db_column='FavMusic', max_length=1000, blank=True,
                                null=True)  # Field name made lowercase.
    favecrivain = models.CharField(db_column='FavEcrivain', max_length=1000, blank=True,
                                   null=True)  # Field name made lowercase.
    favcartoon = models.CharField(db_column='FavCartoon', max_length=1000, blank=True,
                                  null=True)  # Field name made lowercase.
    www = models.CharField(db_column='WWW', max_length=150, blank=True, null=True)  # Field name made lowercase.
    projets = models.TextField(db_column='Projets', blank=True, null=True)  # Field name made lowercase.
    passetemps = models.TextField(db_column='PasseTemps', blank=True, null=True)  # Field name made lowercase.
    autre = models.TextField(db_column='Autre', blank=True, null=True)  # Field name made lowercase.
    biographie = models.TextField(db_column='Biographie', blank=True, null=True)  # Field name made lowercase.
    favsaying = models.CharField(db_column='FavSaying', max_length=1000, blank=True,
                                 null=True)  # Field name made lowercase.
    ppcheck = models.CharField(max_length=3, blank=True, null=True)
    ppfl = models.CharField(max_length=50)
    ppurl = models.CharField(max_length=500, blank=True, null=True)
    ppw = models.PositiveSmallIntegerField(db_column='ppW', blank=True, null=True)  # Field name made lowercase.
    pph = models.PositiveSmallIntegerField(db_column='ppH', blank=True, null=True)  # Field name made lowercase.
    mailinglist = models.CharField(db_column='MailingList', max_length=3)  # Field name made lowercase.
    defaultordering = models.PositiveIntegerField(db_column='DefaultOrdering', blank=True,
                                                  null=True)  # Field name made lowercase.
    tmpl = models.PositiveIntegerField(blank=True, null=True)
    lire1fois = models.CharField(db_column='lire1Fois', max_length=10, blank=True,
                                 null=True)  # Field name made lowercase.
    portfolioorder = models.CharField(db_column='portfolioOrder', max_length=4)  # Field name made lowercase.
    stayconnected = models.CharField(db_column='stayConnected', max_length=1)  # Field name made lowercase.
    mailboxcollapse = models.CharField(db_column='mailboxCollapse', max_length=1)  # Field name made lowercase.
    mailbox_acceptmail = models.CharField(db_column='mailbox_acceptMail', max_length=1)  # Field name made lowercase.
    fuseau = models.CharField(max_length=40)
    uuid = models.CharField(unique=True, max_length=36, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'members'


class MembersNew(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    authorid = models.PositiveIntegerField(db_column='AuthorID', unique=True)  # Field name made lowercase.
    date = models.DateField()

    class Meta:
        managed = False
        db_table = 'members_new'


class Movehistory(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    date = models.DateTimeField()
    moverid = models.PositiveIntegerField(db_column='moverID')  # Field name made lowercase.
    authorid = models.PositiveIntegerField(db_column='authorID')  # Field name made lowercase.
    fromid = models.PositiveIntegerField(db_column='fromID')  # Field name made lowercase.
    fromcat = models.PositiveIntegerField(db_column='fromCat')  # Field name made lowercase.
    fromscat = models.PositiveIntegerField(db_column='fromSCat', blank=True, null=True)  # Field name made lowercase.
    toid = models.PositiveIntegerField(db_column='toID')  # Field name made lowercase.
    tocat = models.PositiveIntegerField(db_column='toCat')  # Field name made lowercase.
    toscat = models.PositiveIntegerField(db_column='toSCat', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'moveHistory'


class Plainte(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    plaignantid = models.PositiveIntegerField(db_column='plaignantID')  # Field name made lowercase.
    poemid = models.PositiveIntegerField(db_column='poemID')  # Field name made lowercase.
    childid = models.PositiveIntegerField(db_column='childID', blank=True, null=True)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='catID')  # Field name made lowercase.
    typeenvoi = models.CharField(db_column='typeEnvoi', max_length=1)  # Field name made lowercase.
    type = models.CharField(max_length=8)
    details = models.TextField(blank=True, null=True)
    etat = models.CharField(max_length=10)
    date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'plainte'


class Plainteactions(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    plainteid = models.PositiveIntegerField(db_column='plainteID')  # Field name made lowercase.
    authorid = models.PositiveIntegerField(db_column='authorID')  # Field name made lowercase.
    date = models.DateTimeField(blank=True, null=True)
    message = models.TextField()

    class Meta:
        managed = False
        db_table = 'plainteActions'


class Poemapprouvhistory(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    poemid = models.PositiveIntegerField(db_column='poemID')  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='catID')  # Field name made lowercase.
    author = models.PositiveIntegerField()
    date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'poemApprouvHistory'


class Populairebrute(models.Model):
    type = models.CharField(max_length=5)
    date = models.DateTimeField()
    memberid = models.PositiveIntegerField(db_column='memberID')  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='catID')  # Field name made lowercase.
    poemid = models.PositiveIntegerField(db_column='poemID')  # Field name made lowercase.
    extra = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'populaireBrute'
        unique_together = (('type', 'memberid', 'catid', 'poemid'),)


class Populairecompil(models.Model):
    poemid = models.PositiveIntegerField(db_column='poemID')  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='catID', primary_key=True)  # Field name made lowercase.
    score = models.FloatField()
    flag = models.CharField(max_length=1)

    class Meta:
        managed = False
        db_table = 'populaireCompil'
        unique_together = (('catid', 'poemid'),)


class Post1(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    uniquereply = models.PositiveIntegerField(db_column='uniqueReply')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.
    modaprouv = models.CharField(db_column='modAprouv', max_length=1)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post1'
        unique_together = (('id', 'catid'),)


class Post10(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    uniquereply = models.PositiveIntegerField(db_column='uniqueReply')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.
    modaprouv = models.CharField(db_column='modAprouv', max_length=1)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post10'
        unique_together = (('id', 'catid'),)


class Post11(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post11'
        unique_together = (('id', 'catid'),)


class Post12(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    uniquereply = models.PositiveIntegerField(db_column='uniqueReply')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.
    modaprouv = models.CharField(db_column='modAprouv', max_length=1)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post12'
        unique_together = (('id', 'catid'),)


class Post13(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    uniquereply = models.PositiveIntegerField(db_column='uniqueReply')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.
    modaprouv = models.CharField(db_column='modAprouv', max_length=1)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post13'
        unique_together = (('id', 'catid'),)


class Post14(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    uniquereply = models.PositiveIntegerField(db_column='uniqueReply')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.
    modaprouv = models.CharField(db_column='modAprouv', max_length=1)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post14'
        unique_together = (('id', 'catid'),)


class Post15(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    uniquereply = models.PositiveIntegerField(db_column='uniqueReply')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.
    modaprouv = models.CharField(db_column='modAprouv', max_length=1)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post15'
        unique_together = (('id', 'catid'),)


class Post16(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post16'
        unique_together = (('id', 'catid'),)


class Post17(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    uniquereply = models.PositiveIntegerField(db_column='uniqueReply')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.
    modaprouv = models.CharField(db_column='modAprouv', max_length=1)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post17'
        unique_together = (('id', 'catid'),)


class Post18(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post18'
        unique_together = (('id', 'catid'),)


class Post19(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post19'
        unique_together = (('id', 'catid'),)


class Post2(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    uniquereply = models.PositiveIntegerField(db_column='uniqueReply')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.
    modaprouv = models.CharField(db_column='modAprouv', max_length=1)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post2'
        unique_together = (('id', 'catid'),)


class Post20(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    uniquereply = models.PositiveIntegerField(db_column='uniqueReply')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.
    modaprouv = models.CharField(db_column='modAprouv', max_length=1)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post20'
        unique_together = (('id', 'catid'),)


class Post21(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    uniquereply = models.PositiveIntegerField(db_column='uniqueReply')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.
    modaprouv = models.CharField(db_column='modAprouv', max_length=1)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post21'
        unique_together = (('id', 'catid'),)


class Post22(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post22'
        unique_together = (('id', 'catid'),)


class Post23(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    fileid = models.SmallIntegerField(db_column='fileID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post23'
        unique_together = (('id', 'catid'),)


class Post24(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    uniquereply = models.PositiveIntegerField(db_column='uniqueReply')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.
    modaprouv = models.CharField(db_column='modAprouv', max_length=1)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post24'
        unique_together = (('id', 'catid'),)


class Post3(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post3'
        unique_together = (('id', 'catid'),)


class Post4(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post4'
        unique_together = (('id', 'catid'),)


class Post5(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    uniquereply = models.PositiveIntegerField(db_column='uniqueReply')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.
    modaprouv = models.CharField(db_column='modAprouv', max_length=1)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post5'
        unique_together = (('id', 'catid'),)


class Post6(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    uniquereply = models.PositiveIntegerField(db_column='uniqueReply')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.
    modaprouv = models.CharField(db_column='modAprouv', max_length=1)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post6'
        unique_together = (('id', 'catid'),)


class Post7(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    uniquereply = models.PositiveIntegerField(db_column='uniqueReply')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.
    modaprouv = models.CharField(db_column='modAprouv', max_length=1)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post7'
        unique_together = (('id', 'catid'),)


class Post8(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    uniquereply = models.PositiveIntegerField(db_column='uniqueReply')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.
    modaprouv = models.CharField(db_column='modAprouv', max_length=1)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post8'
        unique_together = (('id', 'catid'),)


class Post9(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    author = models.PositiveSmallIntegerField(db_column='Author')  # Field name made lowercase.
    postdate = models.DateTimeField(db_column='PostDate', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=7)  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='CatID')  # Field name made lowercase.
    modifiedby = models.PositiveSmallIntegerField(db_column='ModifiedBy')  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.
    scatid = models.IntegerField(db_column='SCatID', blank=True, null=True)  # Field name made lowercase.
    texte = models.TextField(db_column='Texte', blank=True, null=True)  # Field name made lowercase.
    sujet = models.CharField(db_column='Sujet', max_length=250)  # Field name made lowercase.
    hits = models.PositiveIntegerField(db_column='Hits')  # Field name made lowercase.
    lastcommentdate = models.DateTimeField(db_column='LastCommentDate', blank=True,
                                           null=True)  # Field name made lowercase.
    lastcommentid = models.PositiveSmallIntegerField(db_column='LastCommentID')  # Field name made lowercase.
    authorip = models.CharField(db_column='AuthorIP', max_length=15, blank=True,
                                null=True)  # Field name made lowercase.
    replycount = models.PositiveSmallIntegerField(db_column='ReplyCount')  # Field name made lowercase.
    uniquereply = models.PositiveIntegerField(db_column='uniqueReply')  # Field name made lowercase.
    checkmature = models.CharField(db_column='CheckMature', max_length=3)  # Field name made lowercase.
    checkcomment = models.CharField(db_column='CheckComment', max_length=3)  # Field name made lowercase.
    checkvote = models.CharField(db_column='CheckVote', max_length=3)  # Field name made lowercase.
    topscore = models.SmallIntegerField(db_column='TopScore')  # Field name made lowercase.
    awards = models.PositiveIntegerField(db_column='Awards')  # Field name made lowercase.
    notification = models.TextField(db_column='Notification')  # Field name made lowercase.
    modaprouv = models.CharField(db_column='modAprouv', max_length=1)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'post9'
        unique_together = (('id', 'catid'),)


class Pp(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    filename = models.CharField(db_column='FileName', max_length=50, blank=True,
                                null=True)  # Field name made lowercase.
    ext = models.CharField(db_column='Ext', max_length=3)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'pp'


class RatingPoemPrediction(models.Model):
    catid1 = models.PositiveIntegerField(db_column='catID1')  # Field name made lowercase.
    poemid1 = models.PositiveIntegerField(db_column='poemID1')  # Field name made lowercase.
    catid2 = models.PositiveIntegerField(db_column='catID2')  # Field name made lowercase.
    poemid2 = models.PositiveIntegerField(db_column='poemID2')  # Field name made lowercase.
    diff = models.FloatField()
    nbr = models.PositiveIntegerField()

    class Meta:
        managed = False
        db_table = 'rating_poem_prediction'


class RatingPrediction(models.Model):
    userid = models.PositiveIntegerField(db_column='UserID', primary_key=True)  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='catID')  # Field name made lowercase.
    poemid = models.PositiveIntegerField(db_column='poemID')  # Field name made lowercase.
    authorid = models.PositiveIntegerField(db_column='authorID')  # Field name made lowercase.
    rating = models.FloatField()

    class Meta:
        managed = False
        db_table = 'rating_prediction'
        unique_together = (('userid', 'catid', 'poemid'),)


class Ratings(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    raterid = models.PositiveIntegerField(db_column='raterID')  # Field name made lowercase.
    poemid = models.PositiveIntegerField(db_column='poemID')  # Field name made lowercase.
    catid = models.PositiveSmallIntegerField(db_column='catID')  # Field name made lowercase.
    rating = models.PositiveIntegerField()
    prediction = models.FloatField(blank=True, null=True)
    creation = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'ratings'
        unique_together = (('raterid', 'poemid', 'catid'),)


class Recentpoemview(models.Model):
    author = models.PositiveIntegerField()
    from_field = models.PositiveIntegerField(db_column='from')  # Field renamed because it was a Python reserved word.
    poemid = models.PositiveIntegerField(db_column='poemID')  # Field name made lowercase.
    catid = models.PositiveIntegerField(db_column='catID', primary_key=True)  # Field name made lowercase.
    date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'recentPoemView'
        unique_together = (('catid', 'author', 'from_field', 'poemid'),)


class Recentprofilview(models.Model):
    author = models.PositiveIntegerField(primary_key=True)
    from_field = models.PositiveIntegerField(db_column='from')  # Field renamed because it was a Python reserved word.
    date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'recentProfilView'
        unique_together = (('author', 'from_field'),)


class Registerref(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    date = models.DateTimeField(blank=True, null=True)
    ref = models.IntegerField(blank=True, null=True)
    comment = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'registerRef'


class Sections(models.Model):
    uuid = models.CharField(primary_key=True, max_length=36)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=200)
    shortname = models.CharField(unique=True, max_length=30)
    description = models.TextField()
    order = models.IntegerField(unique=True)
    secvisibility = models.IntegerField(db_column='secVisibility')  # Field name made lowercase.
    secadd = models.IntegerField(db_column='secAdd')  # Field name made lowercase.
    secmodify = models.IntegerField(db_column='secModify')  # Field name made lowercase.
    secremove = models.IntegerField(db_column='secRemove')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'sections'


class Sep(models.Model):
    id = models.PositiveSmallIntegerField(db_column='ID', primary_key=True)  # Field name made lowercase.
    sepname = models.CharField(db_column='SepName', max_length=50)  # Field name made lowercase.
    sepid = models.PositiveSmallIntegerField(db_column='SepID')  # Field name made lowercase.
    ordre = models.PositiveSmallIntegerField(db_column='Ordre', unique=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'sep'


class Settings(models.Model):
    id = models.SmallIntegerField(db_column='ID', primary_key=True)  # Field name made lowercase.
    variabule = models.CharField(db_column='Variabule', max_length=10)  # Field name made lowercase.
    valeurn = models.IntegerField(db_column='ValeurN', blank=True, null=True)  # Field name made lowercase.
    valeurc = models.CharField(db_column='ValeurC', max_length=200)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'settings'


class Spamdomains(models.Model):
    domain = models.CharField(primary_key=True, max_length=255)
    createdon = models.DateTimeField(db_column='createdOn')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'spamDomains'


class Spamdomainslog(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    email = models.CharField(max_length=255)
    ip = models.CharField(max_length=20)
    useragent = models.CharField(max_length=500)
    timestamp = models.DateTimeField()
    details = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'spamDomainsLog'


class Suspend(models.Model):
    author = models.PositiveIntegerField(primary_key=True)
    date = models.DateTimeField()
    suspender = models.PositiveIntegerField()
    expiration = models.DateTimeField(blank=True, null=True)
    raison = models.CharField(max_length=255, blank=True, null=True)
    hideidentity = models.IntegerField(db_column='hideIdentity')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'suspend'


class Userlist(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    userid = models.PositiveIntegerField(db_column='userID')  # Field name made lowercase.
    name = models.CharField(max_length=50)
    created = models.DateTimeField()
    lastedit = models.DateTimeField(db_column='lastEdit')  # Field name made lowercase.
    desc = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'userList'


class Userlistmembers(models.Model):
    catid = models.PositiveIntegerField(db_column='catID', primary_key=True)  # Field name made lowercase.
    poemid = models.PositiveIntegerField(db_column='poemID')  # Field name made lowercase.
    listid = models.PositiveIntegerField(db_column='listID')  # Field name made lowercase.
    added = models.DateTimeField()
    desc = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'userListMembers'
        unique_together = (('catid', 'listid', 'poemid'),)


class Vote(models.Model):
    voterid = models.SmallIntegerField(db_column='VoterID', primary_key=True)  # Field name made lowercase.
    voteoption = models.IntegerField(db_column='VoteOption')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'vote'


class Votecompil(models.Model):
    poemid = models.IntegerField(db_column='PoemID')  # Field name made lowercase.
    catid = models.IntegerField(db_column='CatID')  # Field name made lowercase.
    votecount = models.IntegerField(db_column='VoteCount')  # Field name made lowercase.
    hits = models.SmallIntegerField(db_column='Hits')  # Field name made lowercase.
    reply = models.SmallIntegerField(db_column='Reply')  # Field name made lowercase.
    ordre = models.SmallIntegerField(db_column='Ordre', primary_key=True)  # Field name made lowercase.
    lordre = models.SmallIntegerField(db_column='LOrdre')  # Field name made lowercase.
    authorid = models.IntegerField(db_column='AuthorID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'votecompil'


class Votes(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    voterid = models.IntegerField(db_column='VoterID')  # Field name made lowercase.
    poemid = models.IntegerField(db_column='PoemID')  # Field name made lowercase.
    catid = models.IntegerField(db_column='CatID')  # Field name made lowercase.
    voterip = models.CharField(db_column='VoterIP', max_length=15, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'votes'
