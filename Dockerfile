FROM python:3.7

RUN git clone https://gitlab.com/eric.laugier/krysta.git .
RUN pip install -r requirements.txt
RUN apt-get install curl software-properties-common
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install nodejs
RUN node -v
RUN npm -v
RUN cd bulma
RUN npm install
RUN npm run build
RUN cd ../krysta
RUN python manage.py makemigrations
RUN python manage.py migrate
RUN python manage.py compilemessages
RUN python manage.py loaddata init_data/initial_data.json
ENTRYPOINT [ "python", "manage.py" ]
 CMD ["runserver", "80"]

