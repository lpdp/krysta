#  Copyright (c) 2019 - eGen Guru

from django.apps import AppConfig


class LpdpConfig(AppConfig):
    name = 'lpdp'
