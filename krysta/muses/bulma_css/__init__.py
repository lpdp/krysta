# ******************************************************************************
#  Copyright (c) eGen Guru 2019.                                               *
# ******************************************************************************
from django import forms
from django.forms import ModelForm


class BulmaForm(forms.Form):
    def as_bulma(self):
        return self._html_output(
            normal_row='<div class="field" style="display: grid;" %(html_class_attr)s>%(label)s %(field)s%('
                       'help_text)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True,
        )

    def __str__(self):
        return self.as_bulma()


class BulmaModelForm(ModelForm):
    def as_bulma(self):
        return self._html_output(
            normal_row='<div class="field" style="display: grid;" %(html_class_attr)s>%(label)s %(field)s%('
                       'help_text)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True,
        )

    def __str__(self):
        return self.as_bulma()
