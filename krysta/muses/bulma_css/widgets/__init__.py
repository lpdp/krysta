#  Copyright (c) 2019 - eGen Guru
from django.forms import EmailInput, URLInput, PasswordInput, HiddenInput, DateInput, \
    DateTimeInput, TimeInput, Textarea, CheckboxInput

from .BulmaNumberInput import BulmaNumberInput
from .BulmaTextInput import BulmaTextInput


class BulmaEmailInput(EmailInput):
    template_name = 'widgets/email.html'


class BulmaURLInput(URLInput):
    template_name = 'widgets/url.html'


class BulmaPasswordInput(PasswordInput):
    template_name = 'widgets/password.html'

class BulmaHiddenInput(HiddenInput):
    template_name = 'widgets/hidden.html'


class BulmaDateInput(DateInput):
    template_name = 'widgets/date.html'


class BulmaDateTimeInput(DateTimeInput):
    template_name = 'widgets/datetime.html'


class BulmaTimeInput(TimeInput):
    template_name = 'widgets/time.html'


class BulmaTextarea(Textarea):
    template_name = 'widgets/textarea.html'


class BulmaCheckboxInput(CheckboxInput):
    def __init__(self, *args, **kwargs):
        super(BulmaCheckboxInput, self).__init__(*args, **kwargs)
        self.attrs.update({'type': 'checkbox'})

    template_name = 'widgets/checkbox.html'


class BulmaQuillContainer(Textarea):
    template_name = 'widgets/quilljs.html'
