#  Copyright (c) 2019 - eGen Guru
from django.forms import NumberInput


class BulmaNumberInput(NumberInput):
    template_name = 'widgets/number.html'
