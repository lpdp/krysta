#  Copyright (c) 2019 - eGen Guru
from django.forms import TextInput


class BulmaTextInput(TextInput):

    def __init__(self, *args, **kwargs):
        super(BulmaTextInput, self).__init__(*args, **kwargs)
        self.attrs.update({'class': 'input is-primary'})

    template_name = 'widgets/input_text.html'
