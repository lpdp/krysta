#  Copyright (c) 2019 - eGen Guru

import logging
from django.views.generic import TemplateView

from ..models import Section, Tag, License, Member
from ..forms import FormEmailNotificationPreferences, FormMessageNotificationPreferences, KrystaChangePasswordForm

logger = logging.getLogger(__name__)


class TvStandard(TemplateView):
    pass


class FvMultiForms(TemplateView):
    pass


class TvControlCenter(TemplateView):
    def get_context_data(self, **kwargs):
        context = super(TvControlCenter, self).get_context_data(**kwargs)
        context['changePasswordForm'] = KrystaChangePasswordForm()
        return context


class FvMyPreferences(TemplateView):
    form_email_notification = FormEmailNotificationPreferences
    form_message_notification = FormMessageNotificationPreferences

    @staticmethod
    def post(request):
        post_data = request.POST or None

    def get(self, request, *args, **kwargs):
        return self.post(request)


class TvAdminLicenses(TemplateView):
    order_by = "Name"
    items_per_page = 10
    page = 1

    def dispatch(self, request, *args, **kwargs):
        self.order_by = kwargs.get("order_by", self.order_by)
        self.items_per_page = kwargs.get("items_per_page", self.items_per_page)
        self.page = kwargs.get("page", self.page)
        return super(TvAdminLicenses, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(TvAdminLicenses, self).get_context_data(**kwargs)
        context['order_by'] = self.order_by
        context['items_per_page'] = self.items_per_page
        context['page'] = self.page
        context['licenses'] = License.objects.all()
        return context


class TvAdminTags(TemplateView):
    order_by = "Name"
    items_per_page = 10
    page = 1

    def dispatch(self, request, *args, **kwargs):
        self.order_by = kwargs.get("order_by", self.order_by)
        self.items_per_page = kwargs.get("items_per_page", self.items_per_page)
        self.page = kwargs.get("page", self.page)
        return super(TvAdminTags, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(TvAdminTags, self).get_context_data(**kwargs)
        context['tags'] = Tag.objects.all()
        context['order_by'] = self.order_by
        context['items_per_page'] = self.items_per_page
        context['page'] = self.page
        return context


class TvAdminWarnings(TemplateView):
    order_by = "Name"
    items_per_page = 10
    page = 1

    def dispatch(self, request, *args, **kwargs):
        self.order_by = kwargs.get("order_by", self.order_by)
        self.items_per_page = kwargs.get("items_per_page", self.items_per_page)
        self.page = kwargs.get("page", self.page)
        return super(TvAdminWarnings, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(TvAdminWarnings, self).get_context_data(**kwargs)
        context['order_by'] = self.order_by
        context['items_per_page'] = self.items_per_page
        context['page'] = self.page
        return context


class TvAdminWarningTemplates(TemplateView):
    order_by = "Name"
    items_per_page = 10
    page = 1

    def dispatch(self, request, *args, **kwargs):
        self.order_by = kwargs.get("order_by", self.order_by)
        self.items_per_page = kwargs.get("items_per_page", self.items_per_page)
        self.page = kwargs.get("page", self.page)
        return super(TvAdminWarningTemplates, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(TvAdminWarningTemplates, self).get_context_data(**kwargs)
        context['order_by'] = self.order_by
        context['items_per_page'] = self.items_per_page
        context['page'] = self.page
        return context


class TvListMembers(TemplateView):
    order_by = "Pseudonym"
    items_per_page = 10
    page = 1

    def dispatch(self, request, *args, **kwargs):
        self.order_by = kwargs.get("order_by", self.order_by)
        self.items_per_page = kwargs.get("items_per_page", self.items_per_page)
        self.page = kwargs.get("page", self.page)
        return super(TvListMembers, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(TvListMembers, self).get_context_data(**kwargs)
        context['order_by'] = self.order_by
        context['items_per_page'] = self.items_per_page
        context['page'] = self.page
        context['members'] = Member.objects.all()
        return context


class TvListPosts(TemplateView):

    @property
    def section(self):
        return self.kwargs['section']

    order_by = "Last_comment"
    items_per_page = 10
    page = 1
    tags = []

    def dispatch(self, request, *args, **kwargs):
        self.order_by = kwargs.get("order_by", self.order_by)
        self.items_per_page = kwargs.get("items_per_page", self.items_per_page)
        self.page = kwargs.get("page", self.page)
        return super(TvListPosts, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(TvListPosts, self).get_context_data(**kwargs)
        context['section'] = Section.objects.all().filter(short_name=self.section).first()
        context['current_section'] = self.section
        context['order_by'] = self.order_by
        context['items_per_page'] = self.items_per_page
        context['page'] = self.page
        context['posts'] = [
            {
                'title': 'titre 1',
                'summary': 'Life at the moon was the history of tragedy, grabed to a dead particle.',
                'published_at': '2019-01-01 12:34:09',
                'author_ref': {
                    'pseudonym': 'John Smith',
                    'avatar': 'https://bulma.io/images/placeholders/128x128.png'
                },
                'tags': [
                    {'name': 'tag1'},
                    {'name': 'tag2'},
                    {'name': 'tag3'},
                ]
            },
            {
                'title': 'titre 2',
                'summary': 'I raise this ionic cannon, it\'s called lunar flight.',
                'published_at': '2019-02-01 14:38:56',
                'author_ref': {
                    'pseudonym': 'Jane Smith',
                    'avatar': 'https://bulma.io/images/placeholders/128x128.png'
                },
                'tags': [
                    {'name': 'tag1'},
                    {'name': 'tag4'},
                    {'name': 'tag5'},
                ]
            },
            {
                'title': 'titre 3',
                'summary': 'Greatly exaggerated, senior transporters bravely observe a cloudy, final creature.',
                'published_at': '2019-03-30 21:32:02',
                'author_ref': {
                    'pseudonym': 'Jane Smith',
                    'avatar': 'https://bulma.io/images/placeholders/128x128.png'
                },
                'tags': [
                    {'name': 'tag1'},
                    {'name': 'tag6'},
                    {'name': 'tag8'},
                ]
            },
        ]
        return context
