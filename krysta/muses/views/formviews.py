#  Copyright (c) 2019 - eGen Guru

import logging
from django.views.generic import FormView

from ..forms import FormLicense, FormContest

logger = logging.getLogger(__name__)


class FvStandard(FormView):
    pass


class NewLicenseView(FormView):
    template_name = 'license/new_license.html'
    form_class = FormLicense


class NewContestView(FormView):
    template_name = 'contest/new_contest.html'
    form_class = FormContest
