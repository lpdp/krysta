��    -      �  =   �      �     �     �     �     �     �                    (     ,  
   4     ?     K     R     c     r     x     |     �     �     �     �     �     �     �  	   �     �     �     �     �                    "     7     @     G     g     �     �  *   �     �     �     �  @  �     -     /     8     I     P     _     v     |     �  	   �     �     �     �     �     �     �     �     �  	         
               $     ,     5     ;     L     ^     q     u     �     �     �     �     �  	   �      �     	      	     ?	  :   D	  	   	  	   �	     �	         	         "      (               
   &   )         +                    '                 *                 !                      -                                #                 ,   $   %                  A About About us Account Account Inactive Add Tag Admin Administration CGU Confirm Contact us Description E-mail E-mail Addresses E-mail address Email FAQ Frequently Asked Questions Login M Make Primary Members Message Messages Muses My Alerts My Preferences My Warnings Name On new comment On new message Primary Pseudo Re-send Verification Register Remove The name of contest is too long This account is inactive. This writer's name is too long. Type Use this page to find information about us Verified Warning: Warnings Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-10-20 22:22+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 A A propos A propos de nous Compte Compte inactif Ajouter une étiquette Admin Administration CGU Confirmer Contactez-nous Description Courriel Adresse courriel Adresse courriel Courriel FAQ Foire aux questions Connexion M Mettre primaire Membres Message Messages Muses Mes Signalements Mes Préférences Mes Avertissements Nom Sur un nouveau commentaire Sur un nouveau message Primaire Pseudo Renvoyer la vérification Inscription Supprimer Le nom du concours est trop long Ce compte est inactif Le nom de l'auteur est trop long Type Utilisez cette page pour trouver des informations sur nous Vérifié Attention Avertissements 