��    %      D  5   l      @     A     C     I     R     c     k     q     �     �     �     �     �     �  	   �     �     �     �     �     �            	             *     6     >     M     V     f  *   �      �     �     �               #  j  7     �     �     �     �     �     �     �     �     �          !  !   @     b     |     �     �  	   �     �     �     �     �     �     �     �            
   '     2     J  A   `  -   �  /   �  $    	     %	     4	     O	                   !                                                       "   %         #   $                               	                
                                           A About About us Account Inactive Add Tag Admin Administration CGU Contact Create new section Create new tag Create new tag... Disabled at Enable at FAQ Frequently Asked Questions Login M Members Messages Muses My Alerts My Warnings New section New tag OpenID Sign In Register Tags Management This account is inactive. Use this page to find information about us Use this page to manage payments Use this page to manage tags Warning Templates Management Warnings Warnings Management Warnings Moderation Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-10-20 22:22+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 A A propos A propos de nous Compte inactif Ajouter une étiquette Admin Administration CGU Contact Créer une nouvelle section Créer une nouvelle étiquette Créer une nouvelle étiquette... Désactivée à partir de Activée à partir de FAQ Foire Aux Questions Connexion M Membres Messages Muses Mes Signalements Mes Avertissements Nouvelle section Nouvelle étiquette Connexion OpenID S'inscrire Gestion des étiquettes Ce compte est inactif Utilisez cette page to trouver des informations à propos de nous Utiliser cette page pour gérer les paiements Utiliser cette page pour gérer les étiquettes Gestion des modèles d'avertissement Avertissements Gestion des avertissements Modération des avertissements 