#  Copyright (c) 2019 - eGen Guru

from django.urls import path

from .forms import *
from .views import *

urlpatterns = [

    path("",
         TvStandard.as_view(
             template_name="index.html"
         ),
         name="muses_home"
         ),

    path("about/",
         TvStandard.as_view(
             template_name="about.html"
         ),
         name="muses_about"
         ),

    path("admin_warnings/",
         TvAdminWarnings.as_view(
             template_name="warning/admin_warnings.html"
         ),
         name="muses_admin_warnings"
         ),

    path("admin_warning_templates/",
         TvAdminWarningTemplates.as_view(
             template_name="warning_template/admin_warning_templates.html"
         ),
         name="muses_admin_warning_templates"
         ),

    path("admin_licenses/",
         TvAdminLicenses.as_view(
             template_name="license/admin_licenses.html"
         ),
         name="muses_admin_licenses"
         ),

    path("admin_alerts/",
         TvStandard.as_view(
             template_name="alert/admin_alerts.html"
         ),
         name="muses_admin_alerts"
         ),

    path("admin_payments/",
         TvStandard.as_view(
             template_name="payment/admin_payments.html"
         ),
         name="muses_admin_payments"
         ),

    path("admin_contests/",
         TvStandard.as_view(
             template_name="contest/admin_contests.html"
         ),
         name="muses_admin_contests"
         ),

    path("admin_tags/",
         TvAdminTags.as_view(
             template_name="tag/admin_tags.html"
         ),
         name="muses_admin_tags"
         ),

    path("admin_members/",
         TvStandard.as_view(
             template_name="member/admin_members.html"
         ),
         name="muses_admin_members"
         ),

    path("faq/",
         TvStandard.as_view(
             template_name="faq.html"
         ),
         name="muses_faq"
         ),

    path("cgu/",
         TvStandard.as_view(
             template_name="cgu.html"
         ),
         name="muses_cgu"
         ),

    path("members/",
         TvListMembers.as_view(
             template_name="member/list_members.html"
         ),
         name="muses_members"
         ),

    path("contact/",
         FvStandard.as_view(
             form_class=FormContact,
             template_name="contact.html"
         ),
         name="muses_contact"
         ),

    path("my_profile/",
         TvStandard.as_view(
             template_name="my_profile.html"
         ),
         name="muses_my_profile"
         ),

    path("control_center/",
         TvControlCenter.as_view(
             template_name="control_center.html"),
         name="muses_control_center"
         ),

    path("list_posts/<str:section>/",
         TvListPosts.as_view(
             template_name="post/list_posts.html"
         ),
         name="muses_list_posts"
         ),

    path("list_alerts/",
         TvStandard.as_view(
             template_name="alert/list_alerts.html"
         ),
         name="muses_list_alerts"
         ),

    path("list_warnings/",
         TvStandard.as_view(
             template_name="warning/list_warnings.html"
         ),
         name="muses_list_warnings"
         ),

    path("my_messages/", TvStandard.as_view(template_name="message/list_my_messages.html"),
         name="muses_list_my_messages"),
    path("my_warnings/", TvStandard.as_view(template_name="warning/list_my_warnings.html"),
         name="muses_list_my_warnings"),
    path("my_preferences", TvStandard.as_view(
        template_name="my_preferences.html"
    ),
         name="muses_my_preferences"
         ),
    path("view_posts/", TvStandard.as_view(template_name="post/view_post.html"), name="muses_view_post"),
    path("new_license/", NewLicenseView.as_view(), name="muses_new_license"),
    path("new_contest/", NewContestView.as_view(), name="muses_new_contest"),
    path("new_post/", TvStandard.as_view(template_name="post/new_post.html"), name="muses_new_post"),
    path("new_section/",
         FvStandard.as_view(
             form_class=FormSection,
             template_name="section/new_section.html"
         ),
         name="muses_new_section"
         ),
    path("new_license/",
         TvStandard.as_view(
             template_name="license/new_license.html"
         ),
         name="muses_new_license"
         ),
    path("moderation/",
         TvStandard.as_view(
             template_name="moderation.html"
         ),
         name="muses_moderation"
         ),
    path("administration/",
         TvStandard.as_view(
             template_name="administration.html"
         ),
         name="muses_administration"
         ),
]
"""
path("my_preferences/", FvMultiForms.as_view(
    form_class={
        'email_notification': FormEmailNotificationPreferences,
        'message_notification': FormMessageNotificationPreferences},
    template_name="my_preferences.html"),
     name="muses_my_preferences"),
"""
