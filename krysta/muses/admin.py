# ******************************************************************************
#  Copyright (c) eGen Guru 2019.                                               *
# ******************************************************************************

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import MemberCreationForm, MemberChangeForm
from .models import Member, SiteParam


# Register your models here.
class MemberAdmin(UserAdmin):
    add_form = MemberCreationForm
    form = MemberChangeForm
    model = Member
    list_display = ['email', 'username', 'birth_date', ]


class SiteParamAdmin(admin.ModelAdmin):
    list_display = ['key', 'value']


admin.site.register(Member, MemberAdmin)
admin.site.register(SiteParam, SiteParamAdmin)

admin.site.site_header = 'Krysta Administration'
admin.site.site_title = 'Site d\'administration de Kystra'
