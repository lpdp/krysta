#  Copyright (c) 2019 - eGen Guru
from .authforms import *

# ******************************************************************************
#  Copyright (c) eGen Guru 2019.                                               *
# ******************************************************************************

from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.forms import ClearableFileInput
from django.utils.translation import gettext as _
from django.utils.translation import gettext_lazy as lazy_

from ..bulma_css import BulmaForm, BulmaModelForm
from ..bulma_css.widgets import BulmaTextInput, BulmaEmailInput, BulmaTextarea, BulmaPasswordInput, BulmaDateTimeInput, \
    BulmaCheckboxInput
from ..models import Member, License, Contest


class MemberCreationForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = Member
        fields = ('username', 'email', 'pseudonym', 'birth_date')


class MemberChangeForm(UserChangeForm):
    class Meta:
        model = Member
        fields = ('username', 'email', 'pseudonym', 'birth_date')


class FormEmailNotificationPreferences(BulmaForm):
    # Email notification
    email_notification_on_new_comment = forms.BooleanField(
        label=_("On new comment"),
        widget=BulmaCheckboxInput
    )
    email_notification_on_new_message = forms.BooleanField(
        label=_("On new message"),
        widget=BulmaCheckboxInput
    )


class FormMessageNotificationPreferences(BulmaForm):
    # Message notification
    message_notification_on_new_comment = forms.BooleanField(
        label=_("On new comment"),
        widget=BulmaCheckboxInput
    )
    message_notification_on_new_message = forms.BooleanField(
        label=_("On new message"),
        widget=BulmaCheckboxInput
    )


class FormContact(BulmaForm):
    name = forms.CharField(
        label=_("Name"),
        max_length=150,
        widget=BulmaTextInput
    )
    email = forms.EmailField(
        label=_('Email'),
        widget=BulmaEmailInput
    )
    pseudo = forms.CharField(
        label=_('Pseudo'),
        max_length=150,
        widget=BulmaTextInput
    )
    message = forms.CharField(
        label=_('Message'),
        widget=BulmaTextarea,
    )


class FormTag(BulmaForm):
    name = forms.CharField(
        label=_("Name"),
        max_length=255,
        widget=BulmaTextInput
    )
    enable_at = forms.DateTimeField(
        label=_("Enable At"),
        widget=BulmaDateTimeInput
    )
    disable_at = forms.DateTimeField(
        label=_("Disable At"),
        widget=BulmaDateTimeInput
    )
    type = forms.CharField(
        label=_("Type"),
        widget=BulmaTextInput
    )


class FormSection(BulmaForm):
    name = forms.CharField(
        label=_("Name"),
        max_length=255,
        widget=BulmaTextInput
    )
    description = forms.CharField(
        label=_("Description"),
        max_length=2048,
        widget=BulmaTextarea
    )


class FormLicense(BulmaModelForm):
    class Meta:
        model = License
        fields = ['name', 'text', 'logo']
        labels = {
            'name': lazy_('Name'),
            'text': lazy_('Text'),
            'logo': lazy_('Logo')
        }
        help_texts = {
            'name': lazy_('Enter the name of license'),
            'text': lazy_('Enter the text of the license'),
            'logo': lazy_('Upload the logo of the license')
        }
        error_messages = {
            'name': {
                'max_length': _("This writer's name is too long."),
            },
        }
        widgets = {
            'name': BulmaTextInput(),
            'text': BulmaTextarea(),
            'logo': ClearableFileInput()
        }


class FormContest(BulmaModelForm):
    class Meta:
        model = Contest
        fields = ['name']
        labels = {
            'name': lazy_('Name')
        }
        help_texts = {
            'name': lazy_('Enter the name of contest')
        }
        error_messages = {
            'name': {
                'max_length': _('The name of contest is too long')
            }
        }
        widgets = {
            'name': BulmaTextInput()
        }
