#  Copyright (c) 2019 - eGen Guru
from allauth.account.forms import LoginForm, SignupForm, AddEmailForm, ChangePasswordForm, SetPasswordForm, \
    ResetPasswordForm, ResetPasswordKeyForm, PasswordField, SetPasswordField
from django import forms
from django.utils.translation import ugettext as _
from ..bulma_css.widgets import BulmaPasswordInput, BulmaCheckboxInput, BulmaTextInput, BulmaDateInput


class KrystaLoginForm(LoginForm):

    def __init__(self, *args, **kwargs):
        super(KrystaLoginForm, self).__init__(*args, **kwargs)
        self.fields["password"].widget = BulmaPasswordInput()
        self.fields["remember"].widget = BulmaCheckboxInput()

    def login(self, *args, **kwargs):
        # Add your own processing here.

        # You must return the original result.
        return super(KrystaLoginForm, self).login(*args, **kwargs)

    def as_bulma(self):
        """Return this form rendered as HTML <p>s."""
        return self._html_output(
            normal_row='<div class="field" style="display: grid;" %(html_class_attr)s>%(label)s %(field)s%('
                       'help_text)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True,
        )


class KrystaSignupForm(SignupForm):
    birth_date = forms.DateField(
        label=_('Birth Date'),
        widget=BulmaDateInput()
    )

    def __init__(self, *args, **kwargs):
        super(KrystaSignupForm, self).__init__(*args, **kwargs)
        self.fields["email"].widget = BulmaTextInput(attrs={'type': 'email'})
        self.fields["username"].widget = BulmaTextInput()

    def save(self, request):
        # Ensure you call the parent class's save.
        # .save() returns a User object.
        user = super(KrystaSignupForm, self).save(request)

        # Add your own processing here.

        # You must return the original result.
        return user

    def as_bulma(self):
        """Return this form rendered as HTML <p>s."""
        return self._html_output(
            normal_row='<div class="field" style="display: grid;" %(html_class_attr)s>%(label)s %(field)s%('
                       'help_text)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True,
        )


class KrystaAddEmailForm(AddEmailForm):

    def save(self, **kwargs):
        # Ensure you call the parent class's save.
        # .save() returns an allauth.account.models.EmailAddress object.
        email_address_obj = super(KrystaAddEmailForm, self).save(kwargs)

        # Add your own processing here.

        # You must return the original result.
        return email_address_obj

    def as_bulma(self):
        """Return this form rendered as HTML <p>s."""
        return self._html_output(
            normal_row='<div class="field" style="display: grid;" %(html_class_attr)s>%(label)s %(field)s%('
                       'help_text)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True,
        )


class KrystaChangePasswordForm(ChangePasswordForm):

    def __init__(self, *args, **kwargs):
        super(KrystaChangePasswordForm, self).__init__(*args, **kwargs)
        self.fields["oldpassword"].widget = BulmaPasswordInput()
        self.fields["password1"].widget = BulmaPasswordInput()
        self.fields["password2"].widget = BulmaPasswordInput()

    def save(self):
        # Ensure you call the parent class's save.
        # .save() does not return anything
        super(KrystaChangePasswordForm, self).save()

        # Add your own processing here.

    def as_bulma(self):
        """Return this form rendered as HTML <p>s."""
        return self._html_output(
            normal_row='<div class="field" style="display: grid;" %(html_class_attr)s>%(label)s %(field)s%('
                       'help_text)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True,
        )


class KrystaSetPasswordForm(SetPasswordForm):

    def __init__(self, *args, **kwargs):
        super(KrystaSetPasswordForm).__init__(*args, **kwargs)
        self.fields["password1"].widget = BulmaPasswordInput()
        self.fields["password2"].widget = BulmaPasswordInput()

    def save(self):
        # Ensure you call the parent class's save.
        # .save() does not return anything
        super(KrystaSetPasswordForm, self).save()

        # Add your own processing here.

    def as_bulma(self):
        """Return this form rendered as HTML <p>s."""
        return self._html_output(
            normal_row='<div class="field" style="display: grid;" %(html_class_attr)s>%(label)s %(field)s%('
                       'help_text)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True,
        )


class KrystaResetPasswordForm(ResetPasswordForm):
    email = forms.EmailField(
        label=_("E-mail"),
        required=True,
        widget=BulmaTextInput(attrs={
            "type": "email",
            "placeholder": _("E-mail address"),
        })
    )

    def save(self, request, **kwargs):
        # Ensure you call the parent class's save.
        # .save() returns a string containing the email address supplied
        email_address = super(KrystaResetPasswordForm, self).save(request)

        # Add your own processing here.

        # Ensure you return the original result
        return email_address

    def as_bulma(self):
        """Return this form rendered as HTML <p>s."""
        return self._html_output(
            normal_row='<div class="field" style="display: grid;" %(html_class_attr)s>%(label)s %(field)s%('
                       'help_text)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True,
        )


class KrystaResetPasswordKeyForm(ResetPasswordKeyForm):

    def save(self):
        # Add your own processing here.

        # Ensure you call the parent class's save.
        # .save() does not return anything
        super(KrystaResetPasswordKeyForm, self).save()

    def as_bulma(self):
        """Return this form rendered as HTML <p>s."""
        return self._html_output(
            normal_row='<div class="field" style="display: grid;" %(html_class_attr)s>%(label)s %(field)s%('
                       'help_text)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True,
        )
