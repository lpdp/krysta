#  Copyright (c) 2019 - eGen Guru

import datetime
import uuid

from django.contrib.auth.models import AbstractUser
from django.core.files.storage import FileSystemStorage
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import ugettext as translate


def generate_pseudo():
    return str(uuid.uuid4())


########################################################################################################################
class SiteParam(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )
    key = models.CharField(
        verbose_name=translate('key'),
        unique=False,
        null=False,
        blank=False,
        db_index=True,
        max_length=255,
        default=translate("no name")
    )
    value = models.CharField(
        verbose_name=translate("value"),
        unique=True,
        null=False,
        blank=False,
        default=translate('no value'),
        db_index=True,
        max_length=2048
    )


########################################################################################################################
class RecipientGroup(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )


MEMBER_STATUS_CHOICES = [
    ('regular', 'REGULAR'),
    ('privilege', 'PRIVILEGE')
]


def member_avatar_path(instance, filename):
    return 'avatars/member_{0}/{1}'.format(instance.user.id, filename)


########################################################################################################################
class Member(AbstractUser):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )
    birth_date = models.DateField(
        verbose_name=translate('birthdate'),
        unique=False,
        null=False,
        blank=False,
        default=datetime.datetime(year=1979, month=3, day=12),
        db_index=True
    )
    pseudonym = models.CharField(
        verbose_name=translate('pseudonym'),
        max_length=50,
        unique=True,
        null=False,
        blank=False,
        db_index=True,
        default=generate_pseudo
    )
    avatar = models.FileField(
        verbose_name=translate('avatar'),
        upload_to=member_avatar_path,
        storage=FileSystemStorage
    )
    web_site = models.URLField(
        verbose_name=translate('web site'),
        max_length=2048,
    )
    gender = models.CharField(
        verbose_name=translate('gender'),
        max_length=1
    )
    job = models.CharField(
        verbose_name=translate('job'),
        max_length=255
    )
    postal_address = models.CharField(
        verbose_name=translate('postal address'),
        max_length=1024
    )
    postal_address_2 = models.CharField(
        verbose_name=translate('postal address 2'),
        max_length=1024
    )
    postal_city = models.CharField(
        verbose_name=translate('postal city'),
        max_length=200
    )
    postal_code = models.CharField(
        verbose_name=translate('postal code'),
        max_length=20
    )
    connections_counter = models.IntegerField(
        verbose_name=translate('connections counter'),
        default=0,
    )
    page_views_counter = models.IntegerField(
        verbose_name=translate('page views counter'),
        default=0,
    )
    email_notification_on_new_comment = models.BooleanField(
        verbose_name=translate('email notification on new comment'),
        default=False
    )
    email_notification_on_new_message = models.BooleanField(
        verbose_name=translate('email notification on new message'),
        default=False
    )
    story = models.TextField(
        verbose_name=translate('story'),
        default="",
        unique=False,
        blank=False,
        null=False
    )

    recipient_group = models.ManyToManyField(RecipientGroup)


########################################################################################################################
class Section(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )
    short_name = models.CharField(
        verbose_name=translate('short name'),
        unique=True,
        max_length=20,
        default=translate('no short name'),
        db_index=True
    )
    name = models.CharField(
        verbose_name=translate('name'),
        unique=False,
        max_length=255,
        default=translate('no name'),
        db_index=True
    )
    order = models.IntegerField(
        verbose_name=translate('order'),
        unique=True,
        default=0
    )
    description = models.TextField(
        verbose_name=translate('description'),
        max_length=1024,
        null=True
    )


########################################################################################################################
class Tag(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )
    name = models.CharField(
        max_length=255
    )
    enable_at = models.DateTimeField(
        verbose_name=translate('enable at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True
    )
    disable_at = models.DateTimeField(
        verbose_name=translate('disable at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True
    )
    type = models.CharField(
        verbose_name=translate('type'),
        unique=False,
        null=False,
        blank=True,
        db_index=False,
        max_length=255
    )
    mature = models.BooleanField(
        verbose_name=translate('mature'),
        unique=False,
        null=False,
        blank=True,
        default=False
    )
    active = models.BooleanField(
        verbose_name=translate('active'),
        unique=False,
        null=False,
        blank=False,
        default=False
    )

    def __str__(self):
        return self.name


########################################################################################################################
class License(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )
    name: str = models.CharField(
        verbose_name=translate('name'),
        max_length=255,
        null=True
    )
    text = models.TextField(
        verbose_name=translate('text'),
        null=True
    )
    logo = models.ImageField(
        verbose_name=translate('logo'),
        null=True
    )
    active = models.BooleanField(
        verbose_name=translate('active'),
        unique=False,
        null=False,
        blank=False,
        default=False
    )

    def __str__(self):
        return self.name


########################################################################################################################
class Book(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )
    title = models.CharField(
        verbose_name=translate('title'),
        max_length=255
    )
    isbn = models.CharField(
        verbose_name=translate('isbn'),
        max_length=20,
    )
    published_at = models.DateField(
        verbose_name=translate('published date')
    )
    author_ref = models.ForeignKey(
        to=Member,
        on_delete=models.CASCADE,
    )
    license_ref = models.ForeignKey(
        to=License,
        on_delete=models.CASCADE,
    )
    visible = models.BooleanField(
        verbose_name=translate('visible'),
        unique=False,
        null=False,
        blank=False,
        default=False
    )

    def __str__(self):
        return self.title


########################################################################################################################
class BookPart(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )
    title = models.CharField(
        verbose_name=translate('title'),
        unique=False,
        null=False,
        blank=False,
        db_index=True,
        max_length=255
    )
    order = models.IntegerField(
        verbose_name=translate('order'),
        unique=False,
        null=True,
        blank=True,
        db_index=False
    )
    book_ref = models.ForeignKey(
        to=Book,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.title


POST_STATUS_CHOICES = [
    ('draft', translate('DRAFT')),
    ('published', translate('PUBLISHED')),
    ('warned', translate('WARNED'))
]


########################################################################################################################
class Post(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )
    title = models.CharField(
        verbose_name=translate('title'),
        unique=False,
        null=False,
        blank=False,
        max_length=255,
    )
    summary = models.TextField(
        verbose_name=translate('summary'),
        unique=False,
        null=True,
        blank=True,
        max_length=2000,
    )
    content = models.TextField(
        verbose_name=translate('content'),
        unique=False,
        null=True,
        blank=True,
    )
    status = models.CharField(
        verbose_name=translate('status'),
        choices=POST_STATUS_CHOICES,
        max_length=20
    )
    validated_at = models.DateTimeField(
        verbose_name=translate('validated at'),
        unique=False,
        null=True,
        blank=False
    )
    validated_by = models.ForeignKey(
        to=Member,
        on_delete=models.DO_NOTHING,
        related_name="validated_by"
    )
    author_ref = models.ForeignKey(
        to=Member,
        on_delete=models.CASCADE,
        related_name="main_author"
    )
    co_authors = models.ManyToManyField(
        to=Member,
        related_name="coauthors",
    )
    tags = models.ManyToManyField(
        to=Tag
    )

    class Meta:
        permissions = (
            ('self_change_post', 'Can change own post'),
            ('self_delete_post', 'Can delete own post'),
        )


class PostVersion(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True,
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True,
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )
    title = models.CharField(
        verbose_name=translate('title'),
        unique=False,
        null=False,
        blank=False,
        max_length=255,
    )
    summary = models.TextField(
        verbose_name=translate('summary'),
        unique=False,
        null=True,
        blank=True,
        max_length=2000,
    )
    content = models.TextField(
        verbose_name=translate('content'),
        unique=False,
        null=False,
        blank=False,
        db_index=True,
    )
    order = models.PositiveSmallIntegerField(
        default=0
    )
    post_ref = models.ForeignKey(
        to=Post,
        on_delete=models.CASCADE
    )

    class Meta:
        permissions = (
            ('self_delete_post_version', 'Can delete own post version'),
        )


########################################################################################################################
class Comment(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True,
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True,
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )
    content = models.TextField(
        verbose_name=translate('content'),
        unique=False,
        null=False,
        blank=False,
        db_index=True,
    )
    author_ref = models.ForeignKey(
        to=Member,
        on_delete=models.CASCADE,
    )
    post_ref = models.ForeignKey(
        to=Post,
        on_delete=models.CASCADE
    )
    reply_to = models.ForeignKey(
        'self',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    status = models.CharField(
        verbose_name=translate('status'),
        choices=POST_STATUS_CHOICES,
        max_length=20
    )

    class Meta:
        permissions = (
            ('self_change_comment', 'Can change own comment'),
            ('self_delete_comment', 'Can delete own comment'),
        )


########################################################################################################################
class Rating(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True,
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True,
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )
    level = models.PositiveSmallIntegerField(
        verbose_name=translate('level'),
        unique=False,
        null=False,
        blank=False,
        default=0,
        validators=[
            MaxValueValidator(5),
            MinValueValidator(1)
        ],
    )
    owner = models.ForeignKey(
        verbose_name=translate('owner'),
        to=Member,
        on_delete=models.CASCADE,
        null=False,
        blank=False,
    )
    target_post = models.ForeignKey(
        verbose_name=translate('target post'),
        to=Post,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    target_comment = models.ForeignKey(
        verbose_name=translate('target comment'),
        to=Comment,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )

    class Meta:
        permissions = (
            ('self_change_rating', 'Can change own rating'),
            ('self_delete_rating', 'Can delete own rating'),
        )


########################################################################################################################
class Like(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True,
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True,
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )
    level = models.PositiveSmallIntegerField(
        verbose_name=translate('level'),
        unique=False,
        null=False,
        blank=False,
        default=0,
        validators=[
            MaxValueValidator(5),
            MinValueValidator(1)
        ],
    )
    owner = models.ForeignKey(
        to=Member,
        on_delete=models.CASCADE,
        null=False,
        blank=False,
    )
    target_post = models.ForeignKey(
        to=Post,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    target_comment = models.ForeignKey(
        to=Comment,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )

    class Meta:
        permissions = (
            ('self_change_like', 'Can change own like'),
            ('self_delete_like', 'Can delete own like'),
        )


########################################################################################################################
# https://www.vertabelo.com/blog/technical-articles/database-model-for-a-messaging-system
class Message(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True,
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True,
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )
    subject = models.CharField(
        verbose_name=translate('subject'),
        unique=False,
        null=False,
        blank=False,
        db_index=True,
        max_length=255,
    )
    content = models.TextField()
    parent_message = models.ForeignKey(
        'self',
        related_name="parent_msg",
        on_delete=models.CASCADE,
    )
    is_reminder = models.BooleanField(
        verbose_name=translate('is reminder'),
        default=False,
    ),
    next_remind_date = models.DateTimeField(
        verbose_name=translate('next remind date'),
        null=True,
        blank=True,
    ),
    reminder_frequency_id = models.IntegerField(
        verbose_name=translate('reminder frequency'),
    )

    class Meta:
        permissions = (
            ('self_change_message', 'Can change own message'),
            ('self_delete_message', 'Can delete own message'),
        )


########################################################################################################################
class MessageRecipient(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True,
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True,
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )
    is_read = models.BooleanField(
        verbose_name=translate('read'),
        default=False
    )
    message = models.ForeignKey(
        to=Message,
        on_delete=models.CASCADE
    )
    recipient = models.ForeignKey(
        to=Member,
        on_delete=models.CASCADE,
        null=True
    )
    recipient_group = models.ForeignKey(
        to=RecipientGroup,
        on_delete=models.CASCADE
    )


########################################################################################################################
class Payment(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True,
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True,
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )
    owner = models.ForeignKey(
        verbose_name=translate('owner'),
        to=Member,
        on_delete=models.CASCADE
    )
    mode = models.CharField(
        max_length=255,
        unique=False,
        null=False,
        blank=False
    )


########################################################################################################################
class WarningTemplate(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True,
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True,
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )


########################################################################################################################
# noinspection PyShadowingBuiltins
class Warning(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True,
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True,
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )
    title = models.CharField(
        verbose_name=translate('title'),
        unique=False,
        null=False,
        blank=False,
        db_index=True,
        max_length=255,
    )


class WarningAction(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True,
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True,
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )
    warning_ref = models.ForeignKey(
        to=Warning,
        on_delete=models.CASCADE,
        null=False,
        blank=False,
    )


########################################################################################################################
class Notification(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True,
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True,
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )


########################################################################################################################
class Voting(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )


########################################################################################################################
class Vote(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )
    voting_ref = models.ForeignKey(
        to=Voting,
        on_delete=models.CASCADE,
    )


########################################################################################################################
class Alert(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )


########################################################################################################################
class Contest(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )
    name = models.CharField(
        verbose_name=translate('name'),
        unique=False,
        null=False,
        blank=False,
        db_index=True,
        max_length=255,
        default=translate("no name")
    )


########################################################################################################################
class ContestRound(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )
    order = models.PositiveSmallIntegerField(
        verbose_name=translate('order'),
        default=0
    )
    contest_ref = models.ForeignKey(
        to=Contest,
        on_delete=models.CASCADE,
    )


########################################################################################################################
class IPEntry(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )
    ip_address = models.GenericIPAddressField(
        verbose_name=translate('ip address'),
        unique=False,
        null=False,
        blank=False,
        db_index=True,
    )
    member_ref = models.ForeignKey(
        to=Member,
        on_delete=models.CASCADE,
    )


class Faq(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=False,
        blank=False,
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=False,
        blank=False,
        auto_now=True
    )
    deleted_at = models.DateTimeField(
        verbose_name=translate('deleted at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )
    question = models.CharField(
        verbose_name=translate("question"),
        unique=True,
        null=False,
        blank=False,
        default=translate('new question'),
        db_index=True,
        max_length=2048
    )
