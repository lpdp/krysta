# ******************************************************************************
#  Copyright (c) eGen Guru 2019.                                               *
# ******************************************************************************

from django.apps import AppConfig


class MusesConfig(AppConfig):
    name = 'muses'
