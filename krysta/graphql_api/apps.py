# ******************************************************************************
#  Copyright (c) eGen Guru 2019.                                               *
# ******************************************************************************

from django.apps import AppConfig


class GraphqlApiConfig(AppConfig):
    name = 'graphql_api'
