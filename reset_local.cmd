set DJANGO_SETTINGS_MODULE=krysta.settings
del .\krysta\db.sqlite3
del .\krysta\muses\migrations\0*.py
call .\env\scripts\activate.bat
cd krysta
python manage.py makemigrations
python manage.py migrate
python manage.py loaddata .\init_data\initial_data.json
python manage.py makemessages --locale=fr
python manage.py makemessages --locale=fr_FR
python manage.py compilemessages
cd ..
timeout 30
exit 0